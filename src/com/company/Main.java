package com.company;
import java.io.IOException;

public class Main {

    /**
    @param args contains input and output files
     */
    public static void main(String[] args) throws IOException {
        ReadAndWriteToFile rw = null;
        try {
            rw = new ReadAndWriteToFile(args);
            rw.execute();
            rw.close();

        } catch (ArrayIndexOutOfBoundsException e){
            System.out.println("wrong usage");

        } catch (IOException e) {
            System.out.println("bad input");

        } finally {
            rw.close();
            System.exit(0);
        }

    }
}
