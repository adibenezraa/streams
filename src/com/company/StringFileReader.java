package com.company;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
StringFileReader class.
Responsible for reading the input file
 */
public class StringFileReader {

    private BufferedReader reader;
    /**
    constructor.
    @param filename the input file
     */
    public StringFileReader(String filename) throws FileNotFoundException {
        this.reader = new BufferedReader(new FileReader(filename));
    }
    /**
    read line method, read line from the input file using reader
     @return reader.readline
     */
    public String readLine() throws IOException {
        return reader.readLine();
    }
    /**
    close reader
     */
    public void close() throws IOException {
        this.reader.close();
    }

}
