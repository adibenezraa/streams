package com.company;
import java.io.IOException;
import java.net.URL;

/** url class.
contains the url size, and the URL obj.
 */
public class Url {
    private int size;
    private URL url;

    /** constructor.
    @param address the url address
    @throws IOException

    the content length of the resource that this connection's URL references, -1 if
    the content length is not known, or if the content length is greater than Integer.MAX_VALUE.
    init the size according to the content length
     */
    public Url(String address) throws IOException {

        this.url = new URL(address);
        this.size = this.url.openConnection().getContentLength();
        this.url.openStream();

        if (this.size == -1)
            this.size = 0;
    }
    /**
    @return this.size
     */
    public int getSize(){ return this.size; }

    /**
     @return url.tostring + url size
     */
    @Override
    public String toString() { return url.toString() + " " + size; }

}
