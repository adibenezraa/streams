package com.company;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
StringFileWriter class.
Responsible for writing the output file
 */
public class StringFileWriter {
    private BufferedWriter writer;

    public StringFileWriter(String filename) throws IOException {
        writer = new BufferedWriter(new FileWriter(filename));
    }
    /**
    @param urlList the url list that we should write to the output file
     */
    public void write(ArrayList<Url> urlList) throws IOException {
        writer.flush();

        for (Url url: urlList){
            writer.write(url.toString());
            writer.newLine();
        }
    }
    public void close() throws IOException {
        this.writer.close();
    }
}
