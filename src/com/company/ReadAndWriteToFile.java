package com.company;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

/** ReadAndWriteToFileClass.
Responsible for writing and reading a file
 */
public class ReadAndWriteToFile {

    private StringFileReader stringFileReader;
    private StringFileWriter stringFileWriter;
    private ArrayList<Url> urlArrayList = new ArrayList<Url>();

    /** constructor.
    @param args contains input and output files
    @throws IOException
    init the stringFileReader and stringFileWriter
    */
    public ReadAndWriteToFile(String[] args) throws IOException {
        this.stringFileReader = new StringFileReader(args[0]);
        this.stringFileWriter = new StringFileWriter(args[1]);
    }
    /**
     The execute function
     */
    public void execute() throws IOException {

        String line;
        while ((line = this.stringFileReader.readLine()) != null) {

            try { this.urlArrayList.add(new Url(line)); } catch (IOException e) { }
        }
        Collections.sort(this.urlArrayList, new UrlSizeSorter());
        this.stringFileWriter.write(this.urlArrayList);
    }

    public void close() throws IOException {
        this.stringFileWriter.close();
        this.stringFileReader.close();
    }
}
