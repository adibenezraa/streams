package com.company;
import java.util.Comparator;

/**
implements the compare function for the url obj according to the url size
 */
public class UrlSizeSorter implements Comparator<Url> {

    /**
     * @param a url1
     * @param b url2
     * @return a.getSize() - b.getSize()
     */
    public int compare(Url a, Url b) { return a.getSize() - b.getSize() ; }
}
